package main

import (
	"encoding/json"
	"fmt"
	shapesModule "gitlab.com/alikhan.murzayev/lesson-5-module"
	"io/ioutil"
)

func main() {
	jsonFileName := "shapes.json"

	// Read json-file
	dat, err := ioutil.ReadFile(jsonFileName)
	if err != nil {
		panic(err)
	}

	shapes := shapesModule.Shapes{}
	err = json.Unmarshal(dat, &shapes)
	if err != nil {
		panic(err)
	}

	for _, shape := range shapes.Shapes {
		area, err := shape.Area()
		if err != nil {
			shapeJsonBytes, _ := json.Marshal(shape)
			fmt.Println(string(shapeJsonBytes), err)
		} else {
			shapeJsonBytes, _ := json.Marshal(shape)
			fmt.Println(string(shapeJsonBytes), area)
		}
	}

}
